/*! \file BSRecvData.h
    \brief Classe responsavel por gerenciar o recebimento (escutar) das mensagens do hw

*/
#ifndef BSRECVDATA_H
#define BSRECVDATA_H

#include "LibTypes.h"



class BSRecvData
{
    public:
         BSRecvData();
        virtual ~BSRecvData();
    protected:
         /*! \fn int waitForConnection(const char *port, int *clientSocket, int timeout);
        \brief Aguarda a conexao na porta especificada
        \param port Número da porta
        \param clientSocket Armazenara o socket aberto com o cliente, no momento da conexao
        \param timeout tempo de espera
        \return Sucesso ou Erro (veja: MSG_CODES)
       */
        int waitForConnection(const char *port, int *clientSocket, int timeout = DEFAULT_BS_TIMEOUT);

         /*! \fn int recvDataFromClient(const int clientSocket, char *dataBuf, int buflen)
        \brief Recebe os dados que o cliente (ja conectado) enviara.
            Caso o cliente envie um dado maior que o "buflen", novas
            chamadas desta funcao deve ser implementadas.
        \param clientSocket Socket Id do Cliente
        \param dataBuf Armazenara os dados enviados pelo cliente
        \param buflen Tamanho do buffer
        \return Sucesso ou Erro (veja: MSG_CODES)
       */
        int recvDataFromClient(const int clientSocket, char *dataBuf, int buflen);

    private:
        /*! \var MSG_CODES state
        \brief Codigos de sucesso/erro
        */

        MSG_CODES state;
        /*! \var int listenSocket
        \brief Socket de escuta
        */
        int listenSocket;

        /*! \fn void closeSocket(int socketID)
        \brief Encerra um socket
        \param socketID Socket Id
        */
        void closeSocket(int socketID);
};

#endif // BSRECVDATA_H
