/*! \file LibTypes.h
    \brief Aqui estao declarados alguns codigos/mensagens padrao.

*/

#ifndef TYPES_H_INCLUDED
#define TYPES_H_INCLUDED

#include <App_Messages.h>
#include <APCPorts.h>

/*! \def DEBUG_ENABLED
    \brief Se habilitado, imprime mensagens no console.
*/
/*#define DEBUG_ENABLED*/

/*! \def MAX_STRING
    \brief Tamanho de strings
*/
#define MAX_STRING 256

/*! \def SOCKET_ERROR
    \brief Valor padrao para erro de socket
*/
#define SOCKET_ERROR -1

/*! \def DEFAULT_UPORT
    \brief Porta padrao do Dispositivo Hella
*/


/*! \def DEFAULT_BS_TIMEOUT
    \brief Tempo de timeout para o BrickStream
*/
#define DEFAULT_BS_TIMEOUT 60


#define DEFAULT_HELLA_PORT 10072

/*! \def DEFAULT_BS_COUNTER_LISTEN_PORT
    \brief Porta de escuta do Dispositivo BrickStream para receber os contadores
*/

#define DEFAULT_BS_COUNTER_LISTEN_PORT "2102"

/*! \def DEFAULT_BS_COUNTER_ALERT_LISTEN_PORT
    \brief Porta de escuta do Dispositivo BrickStream para receber os eventos de alerta
*/

#define DEFAULT_BS_COUNTER_ALERT_LISTEN_PORT "2101"

/*! \def DEFAULT_BS_COUNTER_EVENTS_LISTEN_PORT
    \brief orta de escuta do Dispositivo BrickStream para receber os eventos de contagem
*/

#define DEFAULT_BS_COUNTER_EVENTS_LISTEN_PORT "2100"

/*! \def DEFAULT_TIMEOUT
    \brief Timeout em segundos
*/
#define DEFAULT_TIMEOUT 5

/*! \def ADDR_SIZE
    \brief Tamanho de um endereco ip
*/
#define ADDR_SIZE 64


/*! \enum MSG_CODES
    \brief CODIGOS DE ERROS
*/
enum MSG_CODES
{
    MSG_ERROR_NONE=0,
    MSG_CONNECTION_CLOSED=101,

    /*ERRORS */
    MSG_ERROR_NOT_INITIALIZED=-1000,
    MSG_ERROR_NULL_POINTER,
    MSG_ERROR_HEADER_INVALID_DATAGRAM,
    MSG_ERROR_HEADER_WRONG_DATAGRAM,
    MSG_ERROR_SET_DATETIME,
    MSG_ERROR_COMMAND_EXEC,
    MSG_ERROR_BAD_FORMAT,

    /* CONNECTION/SOCKET ERRORS */
    MSG_ERROR_CREATE_SOCKET=-2000,
    MSG_ERROR_BIND,
    MSG_ERROR_CONNECT_TO_DEVICE,
    MSG_ERROR_SEND_MESSAGE,
    MSG_ERROR_RECV_MESSAGE,
    MSG_ERROR_CONNECTION_REFUSED,
    MSG_ERROR_ACCEPT,
    MSG_ERROR_LISTEN



};


/*! \struct CounterEvent
    \brief Detalhamento dos eventos de contagem
*/
typedef struct CounterEvent
{
    int enters,exits,objectID;
    int interval;
    char date[MAX_STRING],startTime[MAX_STRING],endTime[MAX_STRING];
    char deviceID[MAX_STRING], deviceName[MAX_STRING], ObjectType[MAX_STRING], name[MAX_STRING];
    int status;    /*!< \brief Status do hardware no momento do evento\n
                    0 = The Brickstream 2200TM was online and collecting data during this particular time interval.
                    The data you are receiving is actual count data received by the sensor.\n
                    1 = The Brickstream 2200TM was offline during this particular time interval.
                    The data you are receiving is being automatically generated as zeros by the sensor.\n
                    2 or 3 = The zone didn’t exist during the time interval being sent but has since been created.
                    This situation can occur when a new zone is created on a Brickstream 2200TM that currently has data
                    cached and ready to send to the server. Once the cached data is sent to the server, all data packets
                    should return to zero status.\n
                    4 = The Tracking Engine was disabled due to quality constraints during this particular time interval
                    and was unable to accurately count.\n
                    */


}CounterEvent;


/*! \struct AlertEvent
    \brief Detalhamento dos alertas de eventos (contagens)
*/
typedef struct AlertEvent
{
    char dateString[MAX_STRING];
    int enters,exits;
    int eventType;   /*!< \brief Tipo do evento\n
                        0 = Entrada\n
                        1 = Saída\n
                        -1 = Erro/Indefinido\n
                        */
}AlertEvent;


#endif // TYPES_H_INCLUDED
