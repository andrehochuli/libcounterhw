#ifndef HELLAMESSAGEHANDLER_H
#define HELLAMESSAGEHANDLER_H
/*! \file HellaMessageHandler.h
    \brief Classe responsavel por gerenciar a troca de mensagens com o dispositivo

*/

extern "C"{
#include "PacketHeader.h"
}

class HellaMessageHandler
{
    public:
        HellaMessageHandler();
        virtual ~HellaMessageHandler();
    protected:
        /*! \fn int HellaSendMessage(const char *msgBuffer, const int bufferSize,const int hSocket);
        \brief Envia uma mensagem ao dispositivo
        \param msgBuffer Mensagem
        \param bufferSize Tamanho da mensagem
        \param hSocket socket ID
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int HellaSendMessage(const char *msgBuffer, const int bufferSize,const int hSocket);

        /*! \fn int HellaRecvMessage(char *msgBuffer, const int msgSize,const int hSocket);
        \brief Envia uma mensagem ao dispositivo
        \param msgBuffer Mensagem
        \param bufferSize Tamanho da mensagem
        \param hSocket socket ID
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int HellaRecvMessage(char *msgBuffer, const int msgSize,const int hSocket);

        /*! \fn int HellaCheckHeaderResponse(const PacketHeader_t *header, const unsigned int PKT_MSG_ID)
        \brief Verifica se os dados enviados estao corretos
        \param header cabecalho da resposta
        \param PKT_MSG_ID ID esperado da resposta
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int HellaCheckHeaderResponse(const PacketHeader_t *header, const unsigned int PKT_MSG_ID);


};

#endif // HELLAMESSAGEHANDLER_H
