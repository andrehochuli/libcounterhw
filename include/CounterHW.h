/*! \file CounterHW.h
    \brief Declaracao da Classe Interface dos contadores. Abstrai o uso dos contadores, obtendo os mesmos escopos de funcoes e funcionamento,
    em ambos os hardwares. No entanto, para funcionalidades especificas de cada hardware, utilize as classes diretamente.

    No hardware hella, a comunicacao parte da lib para o dispositivo.
    Ou seja, o hella fica escutando atraves de uma porta e de acordo com a solicitacao (RequestID),
    retorna os dados.

    No hardware Brickstream, a comunicacao parte do dispositivo para a lib.
    Ou seja, a lib fica escutando através de uma porta especifica e aguarda que o BrickStream envie os dados.
    Para cada tipo de mensagem, existe uma porta especifica.
    Tanto as portas de comunicacao, ips, quanto os ciclos de envio, sao configuraveis via interface web

*/
#ifndef COUNTERHW_H
#define COUNTERHW_H


class CounterHW
{
    public:
        CounterHW() {};
        virtual ~CounterHW() {};

        /*! \fn int retrieveCounter(int *in, int *out)
        \brief Retorna os contadores do dispositivo
        \param int *in  Contagem de Entrada
        \param int *out  Contagem de Saida
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        virtual int retrieveCounter(int *in, int *out) = 0;

         /*! \fn int openConnection(const char *hostAddr, const char *dstAddr,
                           const unsigned int uPort,const unsigned int timeout_sec)
        \brief Estabiliza a conexao com o dispositvo
        \param hostAddr Endereco do host
        \param dstAddr Endereco do dispositivo
        \param uPort porta do dispositivo
        \param timeout_sec timeout de conexao
        \return Sucesso ou Erro (veja: MSG_CODES)
       */
        virtual int openConnection(const char *hostAddr, const char *dstAddr,
                           const unsigned int uPort=DEFAULT_HELLA_PORT,const unsigned int timeout_sec=DEFAULT_HELLA_PORT) = 0;

        /*! \fn void closeConnection()
        \brief Encerra a conexao com o dispositvo
       */
        virtual void closeConnection() = 0;

    protected:
    private:
};

#endif // COUNTERHW_H
