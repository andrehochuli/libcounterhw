/*************************************************************
** (c) 2011 by Hella Aglaia Mobile Vision GmbH
** Author: Sadri Hakki
** No usage without written permission.
** All rights reserved.
** This header file defines message IDs for external messages.
*************************************************************/

#ifndef _APPLICATION_MESSAGE_IDS_H_
#define _APPLICATION_MESSAGE_IDS_H_


//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------
// Message groups:
//----------------------------------------------------------------------------------------------------
/*! Definitions of the groups of messages. Message IDs from zero to 0x0F are reserved. */
#define APC_NM_SERVICE_BASE                     0x100   /*!< First message ID for messages originating from
                                                             Counter Application in normal mode. */
#define APC_NM_SERVICE_EXT_BASE                 0x200   /*!< First message ID for messages originating from
                                                             external user application in normal mode. */
#define APC_MAX_MSG_ID                         (0x900 - 1U) /*!< This is the highest message ID usable. */


//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------
// Message codes originating from communication daemon (ethernetd and configd) to HAGL HMI
//----------------------------------------------------------------------------------------------------

/*! This message send connection information from HAGL communication daemon to an external application if a Socket connection is established. <br>
 *  HAGL communication daemon could be:
 *  <ul><li> &quot;HAGLoIPd -n&quot; for normal mode communication from an external application (e.g. HAGL HMI) </li>
 *  <li>  &quot;HAGLoIPd -s&quot; for service mode communication from an external application (e.g. HAGL HMI) </li></ul>
 *  <B>Payload:</B> struct \ref CounterAppConnectionState_t <br>
 *  <B>Request ID:</B> There is no request message id. This message will be send automatically if an external application established a Socket connection to daemon. <br>
 *  <B>Message group:</B> This is a special message. This message will not be send from CounterApplication, it will be send from a HAGL communication daemon. <br>
 */
#define HAGLHMI_MSG_CONNECTION_STATUS           0xFF    //!< Message originating from HAGL communication daemon to HAGL HMI (handshake)



//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------
// Message codes originating from Counter Application in normal mode:
//----------------------------------------------------------------------------------------------------

/*! This message send the counting results. <br>
 *  <B>Payload:</B> struct \ref CountResult_t <br>
 *  <B>Request ID:</B> \ref NM_GET_CONFIGURATION <br>
 *  <B>Message group:</B> \ref APC_NM_SERVICE_BASE  <br>
 */
#define MC_MSG_COUNT_EVENT              (APC_NM_SERVICE_BASE +    0U)     //!< Packet originating from Counter Application.

/*! This message is a confirmation for normal mode request messages which do not require a response message with special information. <br>
 *  <B>Payload:</B> struct \ref MsgId_t, the request message ID <br>
 *  <B>Request ID:</B> All messages with response ID \ref MC_MSG_NM_PARAM_CHANGED in chapter \ref MI_extNM2App <br>
 *  <B>Message group:</B> \ref APC_NM_SERVICE_BASE  <br>
 */
#define MC_MSG_NM_PARAM_CHANGED         (APC_NM_SERVICE_BASE +    6U)     //!< Packet originating from Counter Application.



//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------
// Message codes originating from external user application in normal mode:
//----------------------------------------------------------------------------------------------------

/*! This message set the device system time. <br>
 *  <B>Payload:</B> struct \ref APCSystemTime_t <br>
 *  <B>Response Msg ID:</B> \ref MC_MSG_NM_PARAM_CHANGED <br>
 *  <B>Message group:</B> \ref APC_NM_SERVICE_EXT_BASE  <br>
 */
#define NM_SET_SYSTEM_TIME              (APC_NM_SERVICE_EXT_BASE +   0U)  //!< Packet originating from host.

/*! This message requests a normal mode response message from CounterApplication. <br>
 *  <B>Payload:</B> struct \ref GetConfigurationRequest_t, the requested response message ID <br>
 *  <B>Response Msg ID:</B> All messages with request ID \ref NM_GET_CONFIGURATION in chapter \ref MI_extNM2Host  <br>
 *  <B>Message group:</B> \ref APC_NM_SERVICE_EXT_BASE  <br>
 */
#define NM_GET_CONFIGURATION            (APC_NM_SERVICE_EXT_BASE +   3U)  //!< Packet originating from host.

/* @Andre */
#define MC_MSG_NM_HEALTH 0x101
#define MC_MSG_NM_STATUS  0x103
#define MC_MSG_SYSTEM_TIME_UTC 0x104
#define NM_RESET_COUNT_RESULTS 0x201
#define NM_START_STOP_COUNTING 0x202
#define NM_SYSTEM_REBOOT 0x204

/* @Andre */

#endif

