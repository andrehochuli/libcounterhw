/*************************************************************
 ** (c) 2011 by Hella Aglaia Mobile Vision GmbH
 ** Author: Sadri Hakki
 ** No usage without written permission.
 ** All rights reserved.
 ** This header file defines the system wide unique packet header structure.
 ** All lengths are in bytes.
 *************************************************************/

#ifndef _PACKET_HEADER_H_
#define _PACKET_HEADER_H_

#include <stdint.h>


// ensure packing byte-wise
#pragma pack(push, 1)


typedef uint32_t MsgId_t;   //!< Type for the message ID.


//! \brief The header of a message.
/*! All messages transferred to or from the APC as well as all messages transferred in the APC
 *  internally consist of a header and payload. The header is described by this struct.
 */
typedef struct {
    uint32_t HeaderLen;       //!< Len of this header (= sizeof(PacketHeader_t)).
    uint32_t Magic;           //!< Shall always be equal PACKET_HEADER_MAGIC
    uint32_t PayloadLen;      //!< Len of the Payload immediately following this header.
    MsgId_t  ID;              //!< Unique ID of the packets represented by this header and payload.
} PacketHeader_t;

#define HEADER_LEN              sizeof(PacketHeader_t)          //!< maximum byte length of common message header
#define MAX_PAYLOAD_SIZE        1024U                           //!< maximum byte length of payload following common header
#define MAX_MSG_LEN             (HEADER_LEN + MAX_PAYLOAD_SIZE) //!< maximum byte length of complete message

#define PACKET_HEADER_MAGIC     0xFA5005AF  //!< The magic number of the head.


#pragma pack(pop)

#endif
