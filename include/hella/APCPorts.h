/*************************************************************
** (c) 2011 by Hella Aglaia Mobile Vision GmbH
** Author: Sadri Hakki
** No usage without written permission.
** All rights reserved.
** This header file defines settings of external communication
** via TCP-IP, especially ports used by HAGL software components.
*************************************************************/

#ifndef _APC_PORTS_H_
#define _APC_PORTS_H_


// ANSI C99 standard
//#include <stdint.h>


/*!  This is the port number for external server component for the HAGL video streaming v1 application. <br>
 *   <B>Note:</B> <br>
 *   HAGL video streaming v1 shall be deprecated equal or greater than firmware release version 1.8.
 *   An RTSP video streaming protocol shall be available in this version.
 */
#define APCDEF_VIDEO_V1_EXTERNAL_PORT    ((uint16_t) 1100U) //!< External server port for HAGL video streaming v1.


/*!  This is the port number for external server component for the HAGL normal mode daemon ("HAGLoIPd -n"). <br>
 *   An external application (e.g. HAGL HMI) is able to establish a socket connection to "HAGLoIPd -n" via IP:\ref APCDEF_EXTERNAL_PORT_NM.
 */
#define APCDEF_EXTERNAL_PORT_NM    ((uint16_t) 10072U) //!< External server port for the HAGL normal mode daemon ("HAGLoIPd -n").


/*!  This is the port number for external server component for the HAGL service mode daemon ("HAGLoIPd -s"). <br>
 *   An external application (e.g. HAGL HMI) is able to establish a socket connection to "HAGLoIPd -s" via IP:\ref APCDEF_EXTERNAL_PORT_SM.
 */
#define APCDEF_EXTERNAL_PORT_SM    ((uint16_t) 10073U) //!< External server port for the HAGL service mode daemon ("HAGLoIPd -s").

#endif
