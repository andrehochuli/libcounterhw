/*************************************************************
** (c) 2011 by Hella Aglaia Mobile Vision GmbH
** Author: Sadri Hakki
** No usage without written permission.
** All rights reserved.
** This header file defines message structures
*************************************************************/

#ifndef _APP_MESSAGES_H_
#define _APP_MESSAGES_H_

#include <stdint.h>
#include <PacketHeader.h>
#include <ApplicationMessageIDs.h>



// ensure packing byte-wise
#pragma pack(push, 1)


typedef struct
{
  PacketHeader_t tHeader;

  uint32_t AppStatus;

} MsgHandshakeResponse_t;


typedef struct
{
  PacketHeader_t tHeader;

  uint32_t uCountIn;  //!< description: IN counting result
  uint32_t uCountOut; //!< description: OUT counting result

} MsgCountEvent_t;


void initMsgCountEvent(MsgCountEvent_t *);


typedef struct
{
  PacketHeader_t tHeader;

  MsgId_t ChangedParamMsgID; //!< for ParamChanged

} MsgNmParamChanged_t;


void initMsgNmParamChanged(MsgNmParamChanged_t *);



typedef struct
{
  PacketHeader_t tHeader;

  uint32_t uLinuxSystemTime; //!< system time ticks (from 1970)

} MsgNmSystemTime_t;


void initMsgNmSystemTime(MsgNmSystemTime_t *, const uint32_t);


typedef struct
{
  PacketHeader_t tHeader;

  MsgId_t  eReqID;    //!< Requested message identifier.
  uint32_t Reserved;

} MsgNmGetConfiguration_t;


void initMsgNmGetConfiguration(MsgNmGetConfiguration_t *);


/* @ANDRE */
typedef struct
{
  PacketHeader_t tHeader;
/*! \var int uint32_t OSDState
    \brief Contains the error code for OSD \n
    0 : OSD state is OK.\n
    1: OSD state is error due to right sensor failure.\n
    2: OSD state is warning due to lower AGC limit has reached. \n
    3: OSD state is error due to lower AGC limit has exceeded.\n
    4: OSD state is warning due to upper AGC limit has reached. \n
    5: OSD state is error due to upper AGC limit has exceeded. \n
    255: OSD state is invalid (do not use this for external purpose). \n
    */
  uint32_t OSDState;

  /*! \var int temperature
    \brief The current temperature value.\n
            unit: 1/1000 degree\n
 */
  int temperature;

/*! \var int uint32_t voltage
    \brief The current voltage value. \n
    unit: mV \n
    Please note: Voltage measurement currently not implemented, value is set
    to fixed value of 3300 mV.
    */
  uint32_t voltage;

} MsgNmGetHealth_t;


void initMsgNmGetHealth_t(MsgNmGetHealth_t *);


typedef struct
{
  PacketHeader_t tHeader;
  MsgId_t ChangedParamMsgID;

} MsgNmSetCounterToZero_t;

void initMsgNmSetCounterToZero_t(MsgNmSetCounterToZero_t *);

typedef struct
{
  PacketHeader_t tHeader;

  int value;

} MsgNmCommand_t;

void initMsgNmCommand(MsgNmCommand_t *, int COMMAND_ID);

typedef struct
{
  PacketHeader_t tHeader;

  int32_t OperatingMode;    //!< 0 - Normal Mode , 1 - Service Mode.
  int32_t CountingMode;
  int32_t CountingState;    //!< 0 - Stopped, 1 - Started.
  uint32_t CustomerID;
  uint32_t deprected1;
  uint32_t deprected2;
  uint32_t deprected3;
  uint16_t deprected4;
  uint16_t deprected5;
  uint8_t deprected6;
  uint8_t deprected7;
  uint8_t deprected8;
  uint8_t deprected9;
  int32_t ErrorFlag;    /*!< \brief Contains the error code for OSD \n
                        0 ‐ No error registered, system state is OK.\n
                        1 ‐ Counting could not be started/stopped.\n
                        2 ‐ Initialization of image processing failed.\n
                        3 ‐ OSD error.\n
                        4 ‐ Currently unused.\n
                        5 ‐ Currently unused.\n
                        6 ‐ Currently unused.\n
                        7 ‐ Currently unused.\n
                        8 ‐ Currently unused.\n
                        */


} MsgNmStatus_t;

void initMsgNmStatus(MsgNmStatus_t *);

/* @ANDRE */




#pragma pack(pop)



#endif /* _APP_MESSAGES_H_ */

