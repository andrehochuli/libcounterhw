/*! \file HellaManager.h
    \brief Classe responsavel por gerenciar todos os eventos com o dispositivo Hella

    No hardware hella, a comunicacao parte da lib para o dispositivo.
    Ou seja, o hella fica escutando atraves de uma porta e de acordo com a solicitacao (RequestID),
    retorna os dados.

*/

#ifndef HELLAMANAGER_H
#define HELLAMANAGER_H


#include "LibTypes.h"
#include "HellaConnect.h"
#include "CounterHW.h"

class HellaManager: public CounterHW, public HellaConnect
{
    public:
        HellaManager();
        virtual ~HellaManager();

        /*! \fn int openConnection(const char *hostAddr, const char *dstAddr,
                           const unsigned int uPort = DEFAULT_HELLA_PORT,const unsigned int timeout_sec = DEFAULT_TIMEOUT)
        \brief Estabiliza a conexao com o dispositvo
        \param hostAddr Endereco do host
        \param dstAddr Endereco do dispositivo
        \param uPort porta do dispositivo
        \param timeout_sec timeout de conexao
        \return Sucesso ou Erro (veja: MSG_CODES)
       */
        int openConnection(const char *hostAddr, const char *dstAddr,
                           const unsigned int uPort = DEFAULT_HELLA_PORT,const unsigned int timeout_sec = DEFAULT_TIMEOUT);

        /*! \fn int setDateAndTime(const uint32_t timestamp)
        \brief Configura o timestamp do dispositivo
        \param const uint32_t timestamp timestamp da hora que se deseja configurar (ex: time(NULL))
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int setDateAndTime(const uint32_t timestamp);

         /*! \fn int resetCounter()
        \brief Reinicializa os contadores.
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int resetCounter();

        /*! \fn int reboot(const uint32_t mode)
        \brief Reinicia (0) ou Desliga (1) o dispositivo
        \param const uint32_t mode Se 0 - Reinicia, se >= 1, Desliga.
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int reboot(const uint32_t mode);

        /*! \fn int startStopCounter(const uint32_t mode)
        \brief Inicia (1) ou pausa (0) a contagem
        \param const uint32_t mode Se 0 - pausa, se >= 1, inicia.
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int startStopCounter(const uint32_t mode);

         /*! \fn int retrieveDateAndTime(uint32_t *timestamp)
        \brief Retorna a timestamp do dispositivo
        \param uint32_t *timestamp Armazena o timestamp do dispositivo
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int retrieveDateAndTime(uint32_t *timestamp);

        /*! \fn int retrieveCounter(int *in, int *out)
        \brief Retorna os contadores do dispositivo
        \param int *in  Contagem de Entrada
        \param int *out  Contagem de Saida
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int retrieveCounter(int *in, int *out);

        /*! \fn int retrieveSystemHealth(MsgNmGetHealth_t *health)
        \brief Retorna informacoes do dispositivo
        \param MsgNmGetHealth_t *health Armazena as informações do dispositivo (OSD, Temperatura, Voltagem)
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int retrieveSystemHealth(MsgNmGetHealth_t *health);

        /*! \fn int retrieveSystemStatus(MsgNmStatus_t *status)
        \brief Retorna os estado das cameras, contagem, etc (Ex: bloqueio de visao, baixa luminosidade)
        \param MsgNmGetHealth_t *status Armazena as informações do dispositivo
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int retrieveSystemStatus(MsgNmStatus_t *status);

        /*! \fn int retrieveConnectionState()
        \brief Retorna o estado da conexão com dispositivo
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int retrieveConnectionState();

        /*! \fn void closeConnection()
        \brief Encerra a conexao com o dispositvo
       */
        void closeConnection();


    protected:
    private:
        /*! \fn int sendCommandToDevice(const uint32_t COMMAND_ID, const uint32_t *value)
        \brief Envia um comando ao dispositivo (Ex: sendCommandToDevice(NM_SYSTEM_REBOOT,&mode))
        \param const uint32_t COMMAND_ID codigo do commando (veja ApplicationMessageIDS.h ou APC-Message-API-v2.70.pdf)
        \param const uint32_t *value valor do parametro (Ex: 1
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int sendCommandToDevice(const uint32_t COMMAND_ID, const uint32_t *value);

};

#endif // HELLAMANAGER_H
