/*! \file BSManager.h
    \brief Classe responsavel por gerenciar todos os eventos com o dispositivo BS

    No hardware Brickstream, a comunicacao parte do dispositivo para a lib.
    Ou seja, a lib fica escutando através de uma porta especifica e aguarda que o BrickStream envie os dados.
    Para cada tipo de mensagem, existe uma porta especifica e ciclo de tempo especifico.
    Tanto as portas de comunicacao, ips, quanto os ciclos de envio,
    sao configuraveis via interface web diretamente no dispositivo
*/



#ifndef BSMANAGER_H

#define BSMANAGER_H

#include "BSRecvData.h"
#include "LibTypes.h"
#include "CounterHW.h"

#include <list>

class BSManager: public CounterHW, public BSRecvData
{
    public:
        BSManager();
        virtual ~BSManager();
        /*! \fn int retrieveCounter(int *in, int *out)
        \brief Aguarda o dispositivo enviar os contadores
        \param int *in Contagem de Entrada
        \param int *out Contagem de Saida
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int retrieveCounter(int *in, int *out);

        /*! \fn int retrieveCounterEvents(std::list <CounterEvent> *events);
        \brief Aguarda os eventos de contagem agrupados por intervalo de tempo
        \param std::list <CounterEvent> *events Listagem dos eventos
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int retrieveCounterEvents(std::list <CounterEvent> *events);


        /*! \fn int waitForAlertEvent(AlertEvent *event)
        \brief Aguarda o dispositivo enviar um alerta de cruzamento de linha
        \param AlertEvent *event Dados do Evento
        \return Sucesso ou Erro (veja: MSG_CODES)
        */
        int waitForAlertEvent(AlertEvent *event);

        /*! \fn int openConnection(const char *hostAddr, const char *dstAddr,
                           const unsigned int uPort,const unsigned int timeout_sec)
        \brief Sem efeito para o BrickStream, Implementado apenas para o desenho da Interface CounterHW.
        Lembrando que no BrickStream, abrimos uma porta de escuta e esperamos que este envie os dados,
        no ciclo de tempo configurado no dispositivo
        */
        int openConnection(const char *hostAddr, const char *dstAddr,
                           const unsigned int uPort,const unsigned int timeout_sec)
        {

           /* Sem efeito para o BrickStream */
           /* Implementado apenas para o desenho da Interface CounterHW*/
           return MSG_ERROR_NONE;
        }

         /*! \fn void closeConnection()
        \brief Sem efeito para o BrickStream, Implementado apenas para o desenho da Interface CounterHW
        */
        void closeConnection()
        {
           /* Sem efeito para o BrickStream */
           /* Implementado apenas para o desenho da Interface CounterHW*/
        }

    protected:


    private:
        int parser(char *val, const char *buffer, const char *tag, const char *endToken="\"");




};

#endif // BSMANAGER_H
