/*! \file HellaConnect.h
    \brief Classe responsavel por gerenciar a conexão com o dispotivo

*/

#ifndef HELLACONNECT_H
#define HELLACONNECT_H


extern "C"{
#include "LibTypes.h"
}

#include "HellaMessageHandler.h"


class HellaConnect: public HellaMessageHandler
{
    public:
        HellaConnect();
        virtual ~HellaConnect();
    protected:

        /*! \fn int openConnection(const char *hostAddr, const char *dstAddr,
                           const unsigned int uPort = DEFAULT_HELLA_PORT,const unsigned int timeout_sec = DEFAULT_TIMEOUT);
        \brief Estabiliza a conexao com o dispositvo
        \param hostAddr Endereco do host
        \param dstAddr Endereco do dispositivo
        \param uPort porta do dispositivo
        \param timeout_sec timeout de conexao em segundos
        \return Sucesso ou Erro (veja: MSG_CODES)
       */
        int openConnection(const char *hostAddr, const char *dstAddr,
                           const unsigned int uPort = DEFAULT_HELLA_PORT,const unsigned int timeout_sec = DEFAULT_TIMEOUT);

        /*! \fn void closeConnection()
        \brief Encerra a conexao com o dispositvo
       */
        void closeConnection();

       /*! \var MSG_CODES state
        \brief Codigos de sucesso/erro
        */

        MSG_CODES state;

       /*! \var int hSocket
        \brief Socket ID
        */
        int hSocket;

    private:
        /*! \var char hostAddr[ADDR_SIZE]
        \brief Endereco do Host
        */
        char hostAddr[ADDR_SIZE];

        /*! \var char dstAddr[ADDR_SIZE]
        \brief Endereco do Dispositivo
        */
        char dstAddr[ADDR_SIZE];

        /*! \var unsigned int uPort
        \brief Porta de conexao com o dispositivo
        */
        unsigned int uPort;

};

#endif // HELLACONNECT_H
