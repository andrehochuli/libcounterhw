/*************************************************************
** (c) 2011 by Hella Aglaia Mobile Vision GmbH
** Author: Sadri Hakki
** No usage without written permission.
** All rights reserved.
** This header file defines message structures
*************************************************************/


#include <App_Messages.h>


void initMsgCountEvent(MsgCountEvent_t *i_pMsg)
{
  i_pMsg->tHeader.HeaderLen = sizeof(PacketHeader_t);
  i_pMsg->tHeader.Magic = PACKET_HEADER_MAGIC;
  i_pMsg->tHeader.PayloadLen = sizeof(MsgCountEvent_t) - sizeof(PacketHeader_t);
  i_pMsg->tHeader.ID = MC_MSG_COUNT_EVENT;
}


void initMsgNmParamChanged(MsgNmParamChanged_t *i_pMsg)
{
  i_pMsg->tHeader.HeaderLen = sizeof(PacketHeader_t);
  i_pMsg->tHeader.Magic = PACKET_HEADER_MAGIC;
  i_pMsg->tHeader.PayloadLen = sizeof(MsgNmParamChanged_t) - sizeof(PacketHeader_t);
  i_pMsg->tHeader.ID = MC_MSG_NM_PARAM_CHANGED;
}

void initMsgNmSystemTime(MsgNmSystemTime_t *i_pMsg,const uint32_t MESSAGE_ID)
{
  i_pMsg->tHeader.HeaderLen = sizeof(PacketHeader_t);
  i_pMsg->tHeader.Magic = PACKET_HEADER_MAGIC;
  i_pMsg->tHeader.PayloadLen = sizeof(MsgNmSystemTime_t) - sizeof(PacketHeader_t);
  i_pMsg->tHeader.ID = MESSAGE_ID;
}

void initMsgNmGetConfiguration(MsgNmGetConfiguration_t *i_pMsg)
{
  i_pMsg->tHeader.HeaderLen = sizeof(PacketHeader_t);
  i_pMsg->tHeader.Magic = PACKET_HEADER_MAGIC;
  i_pMsg->tHeader.PayloadLen = sizeof(MsgNmGetConfiguration_t) - sizeof(PacketHeader_t);
  i_pMsg->tHeader.ID = NM_GET_CONFIGURATION;
}

void initMsgNmGetHealth_t(MsgNmGetHealth_t *i_pMsg)
{
  i_pMsg->tHeader.HeaderLen = sizeof(PacketHeader_t);
  i_pMsg->tHeader.Magic = PACKET_HEADER_MAGIC;
  i_pMsg->tHeader.PayloadLen = sizeof(MsgNmGetHealth_t) - sizeof(PacketHeader_t);
  i_pMsg->tHeader.ID = NM_GET_CONFIGURATION;
}

void initMsgNmSetCounterToZero_t(MsgNmSetCounterToZero_t *i_pMsg)
{
  i_pMsg->tHeader.HeaderLen = sizeof(PacketHeader_t);
  i_pMsg->tHeader.Magic = PACKET_HEADER_MAGIC;
  i_pMsg->tHeader.PayloadLen = sizeof(MsgNmSetCounterToZero_t) - sizeof(PacketHeader_t);
  i_pMsg->tHeader.ID = NM_RESET_COUNT_RESULTS;
}

void initMsgNmCommand(MsgNmCommand_t *i_pMsg, int COMMAND_ID)
{
  i_pMsg->tHeader.HeaderLen = sizeof(PacketHeader_t);
  i_pMsg->tHeader.Magic = PACKET_HEADER_MAGIC;
  i_pMsg->tHeader.PayloadLen = sizeof(MsgNmCommand_t) - sizeof(PacketHeader_t);
  i_pMsg->tHeader.ID = COMMAND_ID;
}

void initMsgNmStatus(MsgNmStatus_t *i_pMsg)
{
  i_pMsg->tHeader.HeaderLen = sizeof(PacketHeader_t);
  i_pMsg->tHeader.Magic = PACKET_HEADER_MAGIC;
  i_pMsg->tHeader.PayloadLen = sizeof(MsgNmStatus_t) - sizeof(PacketHeader_t);
  i_pMsg->tHeader.ID = MC_MSG_NM_STATUS;
}
/* @ANDRE */







