#include "HellaConnect.h"

#include <ctime>
#include <iostream>

#ifdef WIN32
#include <stdio.h>
#include <io.h>
#include <tchar.h>
#include <time.h>
#include <WinSock2.h>

// ANSI C99 compliant internal type definitions
#include <stdint.h>
#pragma warning(push)
#pragma warning(disable : 4996)

#pragma comment(user, "Build from " __DATE__ " at " __TIME__ )
#pragma comment(lib, "ws2_32")

#else
extern "C"{
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
}
#endif


using namespace std;


HellaConnect::HellaConnect()
{
    this->state = MSG_ERROR_NOT_INITIALIZED;
    this->hSocket = SOCKET_ERROR;
}

HellaConnect::~HellaConnect()
{
    this->closeConnection();
}

void HellaConnect::closeConnection()
{


#ifdef WIN32
    if (this->hSocket > 0)
		closesocket(this->hSocket);
#else
    if (this->hSocket > 0)
    {
        /* Stop to read/write */
        shutdown (this->hSocket,SHUT_RDWR);
        /* Close */
        close(this->hSocket);
    }
#endif

    this->hSocket = -1;
}

/* Connect to device and check connection validity */
int HellaConnect::openConnection(const char *inHostAddr, const char *inDstAddr, const unsigned int uPort, const unsigned int timeout_sec)
{
#ifdef WIN32
	const char reusePort = 1;
	SOCKET hSocket = INVALID_SOCKET;
	uint16_t uSocketVer = MAKEWORD( 2, 0 );     // defaults to WinSocket v2.0
	WSADATA wsaData;
	int32_t errCode = WSAStartup(uSocketVer, &wsaData);
	if (errCode != 0)
	{
		printf("Initializing WinSocket interface failed with error code %ld! Abort.\n", errCode);
		return MSG_ERROR_CREATE_SOCKET;
	}
#else
	int reusePort = 1;
	int errCode;

	struct linger l;
    l.l_onoff = 1;
    l.l_linger = 1;
#endif
    struct timeval tv;


    /* Initializing */
    this->state = MSG_ERROR_NONE; //Inicializo com OK, para que as funcoes auxiliares executem
    this->hSocket = SOCKET_ERROR;
    this->uPort = uPort;

    if(!inHostAddr || !inDstAddr)
    {
        this->hSocket = MSG_ERROR_NULL_POINTER;
        return MSG_ERROR_NULL_POINTER;
    }

    memset(this->dstAddr,0,64*sizeof(char));
    memset(this->hostAddr,0,64*sizeof(char));

    strncpy(this->dstAddr,inDstAddr,sizeof(char)*ADDR_SIZE);
    strncpy(this->hostAddr,inHostAddr,sizeof(char)*ADDR_SIZE);

    tv.tv_sec = timeout_sec;  /* 2 Secs Timeout */
    tv.tv_usec = 0;  // Not init'ing this can cause strange errors

    // Create a socket
#ifdef WIN32
	this->hSocket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED); // overlapped version
#else
    this->hSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); // non overlapped version
#endif
	if (SOCKET_ERROR == this->hSocket)
    {
        #ifdef DEBUG_ENABLED
#ifdef WIN32
		cout << "Creating socket failed with error code "  << WSAGetLastError() << "! Abort." << endl;
#else
		cout << "Creating socket failed with error code "  << errno << "! Abort." << endl;
#endif
		#endif
        this->state = MSG_ERROR_CREATE_SOCKET;
        return MSG_ERROR_CREATE_SOCKET;
    }
	/*Reuse*/
	setsockopt(this->hSocket, SOL_SOCKET, SO_REUSEADDR, &reusePort, sizeof(int));

#ifndef WIN32
    /*Reuse*/
	setsockopt(this->hSocket, SOL_SOCKET, SO_REUSEADDR, &reusePort, sizeof(int));
    /*Timeout*/
	setsockopt(this->hSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval));
    /*Closing Behavior*/
	setsockopt(this->hSocket, SOL_SOCKET, SO_LINGER, &l, sizeof(struct linger));
#endif

    /* Set up the sockaddr structure*/
    struct sockaddr_in MyAddress;
#ifdef WIN32
	memset(&MyAddress, 0, sizeof(struct sockaddr_in));
#endif
    MyAddress.sin_family = AF_INET;
    MyAddress.sin_addr.s_addr = inet_addr(this->hostAddr);
    MyAddress.sin_port = htons(uPort);

    /* Bind the listening socket using the information in the sockaddr structure*/
    errCode = bind(this->hSocket, (struct sockaddr *)&MyAddress, sizeof(MyAddress));
    if (SOCKET_ERROR == errCode)
    {
        #ifdef DEBUG_ENABLED
#ifdef WIN32
		cout << "Binding socket failed with error code " << WSAGetLastError() << "! Abort." << endl;
#else
		cout << "Binding socket failed with error code " << errno << "! Abort." << endl;
#endif
        #endif
		closeConnection();
        this->state = MSG_ERROR_BIND;
        return MSG_ERROR_BIND;
    }

    /* connect to Counter device */
    struct hostent *foreignHost = gethostbyname(this->dstAddr);
    char *foreignIP = inet_ntoa(*(struct in_addr *)*foreignHost->h_addr_list);
    struct sockaddr_in CounterAddres;
#ifdef WIN32
	memset(&CounterAddres, 0, sizeof(struct sockaddr_in));
#endif
    CounterAddres.sin_family = AF_INET;
    CounterAddres.sin_addr.s_addr = inet_addr(foreignIP);
    CounterAddres.sin_port = htons(uPort);

	errCode = connect(this->hSocket, (struct sockaddr *)&CounterAddres, sizeof(CounterAddres));
    if (SOCKET_ERROR == errCode)
    {
      #ifdef DEBUG_ENABLED
#ifdef WIN32
		cout << "Connecting the Counter device failed with error code " << WSAGetLastError() << "! Abort." << endl;
#else
		cout << "Connecting the Counter device failed with error code " << errno << "! Abort." << endl;
#endif
      #endif
	  closeConnection();
      this->state = MSG_ERROR_CONNECT_TO_DEVICE;
      return MSG_ERROR_CONNECT_TO_DEVICE;
    }

    #ifdef DEBUG_ENABLED
    cout << "Physical connection to target device established." << endl;
    #endif

    /*checking  the physical/logical integrity*/
    uint8_t *msgRawBuffer[MAX_MSG_LEN];
    memset(msgRawBuffer, 0, sizeof(msgRawBuffer));
    this->state = (MSG_CODES)this->HellaRecvMessage((char *)&msgRawBuffer,sizeof(MsgHandshakeResponse_t),this->hSocket);
    if(this->state != MSG_ERROR_NONE)
        return this->state;

    const MsgHandshakeResponse_t *packetResponseHS = (const MsgHandshakeResponse_t *)msgRawBuffer;
    this->state = (MSG_CODES)this->HellaCheckHeaderResponse(&packetResponseHS->tHeader,HAGLHMI_MSG_CONNECTION_STATUS);
    if(this->state != MSG_ERROR_NONE)
        return this->state;

    #ifdef DEBUG_ENABLED
    cout << "Application State: " << packetResponseHS->AppStatus << endl;
    cout << "Target successful verified as Counter device" << endl;
    #endif

    this->state = MSG_ERROR_NONE;

    return MSG_ERROR_NONE;
}
