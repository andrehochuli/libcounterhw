#include "HellaMessageHandler.h"


#include <ctime>
#include <iostream>

#ifdef WIN32
#include <stdio.h>
#include <io.h>
#include <tchar.h>
#include <time.h>
#include <WinSock2.h>
#include "LibTypes.h"

// ANSI C99 compliant internal type definitions
#include <stdint.h>
#pragma warning(push)
#pragma warning(disable : 4996)

#pragma comment(user, "Build from " __DATE__ " at " __TIME__ )
#pragma comment(lib, "ws2_32")

#else
extern "C" {
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include "LibTypes.h"

}
#endif

#include "LibTypes.h"

using namespace std;

HellaMessageHandler::HellaMessageHandler()
{

}

HellaMessageHandler::~HellaMessageHandler()
{

}

int HellaMessageHandler::HellaSendMessage(const char *msgBuffer, const int msgSize,const int hSocket)
{
    if(!msgBuffer)
        return MSG_ERROR_NULL_POINTER;

    if (SOCKET_ERROR == send(hSocket, msgBuffer, msgSize, 0))
    {
      #ifdef DEBUG_ENABLED
      cout << "Unable to send request to device! Abort" << endl;
      #endif

      return MSG_ERROR_SEND_MESSAGE;
    }

    return MSG_ERROR_NONE;
}

int HellaMessageHandler::HellaRecvMessage(char *msgBuffer, const int msgSize,const int hSocket)
{
#ifdef WIN32
    if(!msgBuffer)
        return MSG_ERROR_NULL_POINTER;

    int lenReceived=0;

    do {
    // read and wait until enough received data available
    lenReceived += recv(hSocket, msgBuffer, msgSize - lenReceived, 0);
	if ((0 == lenReceived) || (WSAECONNRESET == lenReceived))
	{
      #ifdef DEBUG_ENABLED
      cout << endl << "Connection lost! Abort" << endl;
      #endif

      return MSG_ERROR_RECV_MESSAGE;
    }
  } while ((lenReceived != SOCKET_ERROR) && (lenReceived < msgSize));

  return MSG_ERROR_NONE;
#else
    if(!msgBuffer)
        return MSG_ERROR_NULL_POINTER;

    int lenReceived=0;

    do {
    // read and wait until enough received data available
    lenReceived += recv(hSocket, msgBuffer, msgSize - lenReceived, 0);
    if (lenReceived <= 0)
    {
      #ifdef DEBUG_ENABLED
      cout << endl << "Connection lost! Abort" << endl;
      #endif

      return MSG_ERROR_RECV_MESSAGE;
    }
  } while ((lenReceived != SOCKET_ERROR) && (lenReceived < msgSize));

  return MSG_ERROR_NONE;
#endif
}


int HellaMessageHandler::HellaCheckHeaderResponse(const PacketHeader_t *header, const unsigned int PKT_MSG_ID)
{
    if(!header)
        return MSG_ERROR_NULL_POINTER;

     if (header->Magic != PACKET_HEADER_MAGIC)
    {
      #ifdef DEBUG_ENABLED
      cout << "Received datagram is invalid. Connection closed" << endl;
      #endif

      return MSG_ERROR_HEADER_INVALID_DATAGRAM;
    }

    if (header->ID != PKT_MSG_ID)
    {
      #ifdef DEBUG_ENABLED
      cout << "Received datagram is of wrong type" << endl;
      #endif

      return MSG_ERROR_HEADER_WRONG_DATAGRAM;
    }

    return MSG_ERROR_NONE;
}

