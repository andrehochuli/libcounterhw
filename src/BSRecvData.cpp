#include "BSRecvData.h"

#include <ctime>
#include <iostream>

#ifdef WIN32
#include <stdio.h>
#include <io.h>
#include <tchar.h>
#include <time.h>
#include <WinSock2.h>

// ANSI C99 compliant internal type definitions
#include <stdint.h>
#pragma warning(push)
#pragma warning(disable : 4996)

#pragma comment(user, "Build from " __DATE__ " at " __TIME__ )
#pragma comment(lib, "ws2_32")

#else
extern "C"{
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
}
#endif


using namespace std;

#define MAXCONN 10
#define DEFAULT_BUFLEN 2048
#define SEND_BUFLEN 128

BSRecvData::BSRecvData()
{
    //ctor
}

BSRecvData::~BSRecvData()
{
    //dtor
}

void BSRecvData::closeSocket(int socketID)
{
#ifdef WIN32
    if (socketID > 0)
		closesocket(socketID);
#else
    if (socketID > 0)
    {
        /* Stop to read/write */
        shutdown (socketID,SHUT_RDWR);
        /* Close */
        close(socketID);
    }
#endif

    socketID = -1;
}

int BSRecvData::recvDataFromClient(const int clientSocket, char *dataBuf, int buflen)
{
    char sendBuf[SEND_BUFLEN];
    int iResult,iSendResult;
    #ifdef DEBUG_ENABLED
    cout << "\t-> Waiting for data...";
    #endif

    memset(dataBuf,0,DEFAULT_BUFLEN);

    iResult = recv(clientSocket, dataBuf, buflen, 0);
    //cout << "iResult " << iResult << endl;
    if (iResult > 0)
    {
        //cout << "Recv: " << dataBuf << endl;
         memset(sendBuf,0,SEND_BUFLEN);
         /*Mensagem de OK para o Cliente*/
         strcpy(sendBuf,"HTTP/1.1 200 OK\r\n\r\n");

        iSendResult = send(clientSocket, sendBuf, strlen(sendBuf), 0 );
        if (iSendResult == SOCKET_ERROR) {

            #ifdef DEBUG_ENABLED
            cout << "\t\t\t[-] send() fail!" << endl;
            #endif

            closeSocket(clientSocket);

            return MSG_ERROR_SEND_MESSAGE;
        }

        #ifdef DEBUG_ENABLED
        cout << "\t[+] Sucesso! Bytes enviados: " <<  iSendResult << endl;
        #endif


    }
    else if (iResult == 0)
    {
       #ifdef DEBUG_ENABLED
       cout << "\t\t[-] Client close connection!\n" << endl;
       #endif
       return MSG_CONNECTION_CLOSED;
    }
    else  {
        #ifdef DEBUG_ENABLED
        cout << "\t\t[-] recv() fail! " << endl ;
        #endif

        closeSocket(clientSocket);

        return MSG_ERROR_RECV_MESSAGE;
    }

    return MSG_ERROR_NONE;
}

int BSRecvData::waitForConnection(const char *port, int *clientSocket, int timeout)
{

#ifdef WIN32
	const char reusePort = 1;
	SOCKET listenSocket = INVALID_SOCKET;
	uint16_t uSocketVer = MAKEWORD( 2, 0 );     // defaults to WinSocket v2.0
	WSADATA wsaData;
	int32_t errCode = WSAStartup(uSocketVer, &wsaData);
	if (errCode != 0)
	{
	    #ifdef DEBUG_ENABLED
		cout << "Initializing WinSocket interface failed with error code " << errCode "Abort." << endl'';
		#endif
		return MSG_ERROR_CREATE_SOCKET;
	}
#else
	int reusePort = 1;
	int errCode;

	struct linger l;
    l.l_onoff = 1;
    l.l_linger = 1;
#endif

    struct timeval tv;
    int iResult;
    tv.tv_sec = timeout;
    tv.tv_usec = 0;  // Not init'ing this can cause strange errors
    /* Initializing */
    this->state = MSG_ERROR_NONE; //Inicializo com OK, para que as funcoes auxiliares executem
    this->listenSocket = SOCKET_ERROR;

    struct addrinfo *result = NULL;
    struct addrinfo hints;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    // Resolvendo o endereco e porta do servidor
    iResult = getaddrinfo(NULL, port, &hints, &result);
    if ( iResult != 0 ) {

        #ifdef DEBUG_ENABLED
        cout << "Getaddrinfo failed with error: " << iResult << endl;
        #endif

        #ifdef WIN32
        WSACleanup();
        #endif

        return MSG_ERROR_CREATE_SOCKET;
    }

    // Create a socket
#ifdef WIN32
	this->listenSocket = WSASocket(result->ai_family, result->ai_socktype, result->ai_protocol, NULL, 0, WSA_FLAG_OVERLAPPED); // overlapped version
#else
    this->listenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol); // non overlapped version
#endif
	if (SOCKET_ERROR == this->listenSocket)
    {
        #ifdef DEBUG_ENABLED
#ifdef WIN32
		cout << "Creating socket failed with error code "  << WSAGetLastError() << "! Abort." << endl;
#else
		cout << "Creating socket failed with error code "  << errno << "! Abort." << endl;
#endif
		#endif
        this->state = MSG_ERROR_CREATE_SOCKET;
        return MSG_ERROR_CREATE_SOCKET;
    }
	/*Reuse*/
	setsockopt(this->listenSocket, SOL_SOCKET, SO_REUSEADDR, &reusePort, sizeof(int));

#ifndef WIN32
    /*Reuse*/
	setsockopt(this->listenSocket, SOL_SOCKET, SO_REUSEADDR, &reusePort, sizeof(int));
    /*Timeout*/
	setsockopt(this->listenSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval));
    /*Closing Behavior*/
	setsockopt(this->listenSocket, SOL_SOCKET, SO_LINGER, &l, sizeof(struct linger));
#endif

    /* Bind the listening socket using the information in the sockaddr structure*/
    errCode = bind(this->listenSocket, result->ai_addr, (int)result->ai_addrlen);
    if (SOCKET_ERROR == errCode)
    {
        #ifdef DEBUG_ENABLED
#ifdef WIN32
		cout << "Binding socket failed with error code " << WSAGetLastError() << "! Abort." << endl;
#else
		cout << "Binding socket failed with error code " << errno << "! Abort." << endl;
#endif
        #endif
            freeaddrinfo(result);

        this->state = MSG_ERROR_BIND;
        return MSG_ERROR_BIND;
    }

    freeaddrinfo(result);

    errCode = listen(this->listenSocket, MAXCONN);
    if (errCode < 0)
	{
	    #ifdef DEBUG_ENABLED
		cout <<"Listen Error: " << errCode << endl;
		#endif

		return MSG_ERROR_LISTEN;
	}

    #ifdef DEBUG_ENABLED
    cout << "Waiting connections at Port: " << port << endl;
    #endif

    *clientSocket = accept(this->listenSocket, NULL, NULL);
    if (*clientSocket <= 0) {

        #ifdef DEBUG_ENABLED
        cout << "Accept Error" << *clientSocket << endl;
        #endif

        closeSocket(this->listenSocket);

        return MSG_ERROR_ACCEPT;
    }

    #ifdef DEBUG_ENABLED
    cout << "Connection Requested..." << endl;
    #endif

    closeSocket(this->listenSocket);

    this->state = MSG_ERROR_NONE;

    return MSG_ERROR_NONE;
}
