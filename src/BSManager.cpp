#include "BSManager.h"


#include <cstdlib>
#include <iostream>
#include <cstring>
#include <ctime>
#include <list>

using namespace std;

#define DEFAULT_BUFLEN 2048

BSManager::BSManager():BSRecvData()
{
    //ctor
}

BSManager::~BSManager()
{
    //dtor
}


int BSManager::parser(char *val, const char *buffer, const char *tag, const char *endToken)
{
    char * tok;
    char msg[MAX_MSG_LEN];
    string str(buffer);

    /* Recupera a tag*/
    /* Ex: counter="123" ==> tag = "counter=\""*/
    size_t pos = str.find(tag);
    if(pos == string::npos)
    {
        #ifdef DEBUG_ENABLED
        cout << "Cannot find tag " << tag << endl;
        #endif
        return MSG_ERROR_BAD_FORMAT;
    }

    string str3 = str.substr (pos+strlen(tag));
    if(str3.size() <= 0)
    {
        #ifdef DEBUG_ENABLED
        cout << "Erro while parsing" << tag << "tag" << endl;
        #endif
        return MSG_ERROR_BAD_FORMAT;
    }


    /* Recupera o conteudo da tag*/
    /* Ex: counter=="123" ==> 123*/
    strncpy(msg,str3.c_str(),MAX_MSG_LEN);
    tok = strtok(msg,endToken);

    if(!tok)
    {
        #ifdef DEBUG_ENABLED
        cout << "Erro while parsing" << tag << "tag" << endl;
        #endif
        return MSG_ERROR_BAD_FORMAT;
    }

    strcpy(val,tok);

    return MSG_ERROR_NONE;
}

int BSManager::retrieveCounter(int *in, int *out)
{

    if(!in || !out)
    {
        #ifdef DEBUG_ENABLED
        cout << "Null pointer to counter pointers" << endl;
        #endif
        return MSG_ERROR_NULL_POINTER;
    }

    char recvbuf[DEFAULT_BUFLEN], counter[16];
    int ret=0, clientSocket=0;

    ret = this->BSRecvData::waitForConnection(DEFAULT_BS_COUNTER_LISTEN_PORT,&clientSocket,DEFAULT_BS_TIMEOUT);
    if(ret != MSG_ERROR_NONE)
    {
        #ifdef DEBUG_ENABLED
        cout << "Erro while waiting for connections" << endl;
        #endif
        return ret;
    }

    ret = this->BSRecvData::recvDataFromClient(clientSocket,recvbuf,DEFAULT_BUFLEN);
    if(ret != MSG_ERROR_NONE)
    {
        #ifdef DEBUG_ENABLED
        cout << "Erro while receiving counters from device" << endl;
        #endif
        return ret;
    }
    //cout << "Recv" << endl << recvbuf << endl;

    /*Mensagem Padrao*/
    /*
    .....
    <RTCount  TotalEnters="493" TotalExits="488"/>
    ....
    */

    ret = parser(counter,recvbuf,(char *)"TotalEnters=\"");
    if(ret != MSG_ERROR_NONE)
        return ret;

    *in = atoi(counter);

    ret = parser(counter,recvbuf,(char *)"TotalExits=\"");
    if(ret != MSG_ERROR_NONE)
          return ret;


    *out = atoi(counter);

    return MSG_ERROR_NONE;
}

int BSManager::retrieveCounterEvents(std::list <CounterEvent> *events)
{
    if(!events)
    {
        #ifdef DEBUG_ENABLED
        cout << "Null pointer to events list" << endl;
        #endif
        return MSG_ERROR_NULL_POINTER;
    }

    CounterEvent evt;

    char recvbuf[DEFAULT_BUFLEN], str[MAX_STRING];
    int ret=0, clientSocket=0;

    ret = this->BSRecvData::waitForConnection(DEFAULT_BS_COUNTER_EVENTS_LISTEN_PORT,&clientSocket,DEFAULT_BS_TIMEOUT);
    if(ret != MSG_ERROR_NONE)
    {
        #ifdef DEBUG_ENABLED
        cout << "Erro while waiting for connections" << endl;
        #endif
        return ret;
    }

    while(ret == MSG_ERROR_NONE && ret != MSG_CONNECTION_CLOSED)
    {
        ret = this->BSRecvData::recvDataFromClient(clientSocket,recvbuf,DEFAULT_BUFLEN);
        if(ret != MSG_ERROR_NONE && ret != MSG_CONNECTION_CLOSED)
        {
            #ifdef DEBUG_ENABLED
            cout << "Erro while receiving counter Events from device" << endl;
            #endif
            return ret;
        }


        /* Recupera os dados dos eventos*/
        /* Mensagem Padrao */
        /*
        <Object Id="0" DeviceId="Device ID" DeviceName="Device Name" ObjectType="0" Name="New Zone">
        <Count StartTime="13:32:00" EndTime="13:45:00" Enters="1" Exits="4" Status="0"/>
        */

        memset(&evt,0,sizeof(CounterEvent));
        ret = parser(str,recvbuf,(char *)"ReportData Interval=\"");
        if(ret != MSG_ERROR_NONE)
           return ret;

        evt.interval = atoi(str);

        ret = parser(str,recvbuf,(char *)"Report Date=\"");
        if(ret != MSG_ERROR_NONE)
            return ret;

        strncpy(evt.date,str,MAX_STRING);

        ret = parser(str,recvbuf,(char *)"Object Id=\"");
        if(ret != MSG_ERROR_NONE)
            return ret;

        evt.objectID = atoi(str);

        ret = parser(str,recvbuf,(char *)"DeviceId=\"");
        if(ret != MSG_ERROR_NONE)
            return ret;

        strncpy(evt.deviceID,str,MAX_STRING);

        ret = parser(str,recvbuf,(char *)"DeviceName=\"");
        if(ret != MSG_ERROR_NONE)
            return ret;

        strncpy(evt.deviceName,str,MAX_STRING);

        ret = parser(str,recvbuf,(char *)"ObjectType=\"");
        if(ret != MSG_ERROR_NONE)
            return ret;

        strncpy(evt.ObjectType,str,MAX_STRING);

        /*" Name" com espaco para diferenciar do "DeviceName"*/
        ret = parser(str,recvbuf,(char *)" Name=\"");
        if(ret != MSG_ERROR_NONE)
            return ret;

        strncpy(evt.name,str,MAX_STRING);

        ret = parser(str,recvbuf,(char *)"StartTime=\"");
        if(ret != MSG_ERROR_NONE)
            return ret;

        strncpy(evt.startTime,str,MAX_STRING);

        ret = parser(str,recvbuf,(char *)"EndTime=\"");
        if(ret != MSG_ERROR_NONE)
            return ret;

        strncpy(evt.endTime,str,MAX_STRING);

        ret = parser(str,recvbuf,(char *)"Enters=\"");
        if(ret != MSG_ERROR_NONE)
            return ret;


        evt.enters = atoi(str);

        ret = parser(str,recvbuf,(char *)"Exits=\"");
        if(ret != MSG_ERROR_NONE)
            return ret;

        evt.exits = atoi(str);


        ret = parser(str,recvbuf,(char *)"Status=\"");
        if(ret != MSG_ERROR_NONE)
            return ret;

        evt.status = atoi(str);

        events->push_back(evt);

    }


    return MSG_ERROR_NONE;
}

int BSManager::waitForAlertEvent(AlertEvent *event)
{
    char recvbuf[DEFAULT_BUFLEN], msg[64];
    int ret=0, clientSocket=0;

    if(!event)
    {
        #ifdef DEBUG_ENABLED
        cout << "Null pointer to event pointer" << endl;
        #endif
        return MSG_ERROR_NULL_POINTER;
    }

    (*event).eventType = -1; //Indefinido

    memset(msg,0,sizeof(char)*64);
    memset(recvbuf,0,sizeof(char)*DEFAULT_BUFLEN);

    ret = this->BSRecvData::waitForConnection(DEFAULT_BS_COUNTER_ALERT_LISTEN_PORT,&clientSocket,DEFAULT_BS_TIMEOUT);
    if(ret != MSG_ERROR_NONE)
    {
        #ifdef DEBUG_ENABLED
        cout << "Erro while waiting for connections" << endl;
        #endif
        return ret;
    }

    ret = this->BSRecvData::recvDataFromClient(clientSocket,recvbuf,DEFAULT_BUFLEN);
    if(ret != MSG_ERROR_NONE)
    {
        #ifdef DEBUG_ENABLED
        cout << "Erro while receiving counters from device" << endl;
        #endif
        return ret;
    }


    /*cout << "Recv" << endl << recvbuf << endl;*/

    /* Mensagem Padrao */
    /*
    GET /EntradaShowRoom?eventType=Enter&Name=New Zone&SiteID=Site ID&DeviceID=Device ID&DT=2015-01-05T14:41:44&Enters=493&Exits=487 HTTP/1.1
    Host: 192.168.234.178:2101
    Content-Length: 0
    Connection: Keep-Alive
    */

    /* Recupero a data*/
    ret = parser(msg,recvbuf,(char *)"&DT=","&");
    if(ret != MSG_ERROR_NONE)
        return ret;

    strncpy((*event).dateString,msg,256);

    /* Recupero o tipo do evento*/
    ret = parser(msg,recvbuf,(char *)"eventType=","&");
    if(ret != MSG_ERROR_NONE)
        return ret;

    if(strcmp("Enter",msg) == 0)
        (*event).eventType = 0;
    else if(strcmp("Exit",msg) == 0)
        (*event).eventType = 1;
    else
        (*event).eventType = -1;

    /* Recupero o total da entrada*/
    ret = parser(msg,recvbuf,(char *)"&Enters=","&");
    if(ret != MSG_ERROR_NONE)
        return ret;

    (*event).enters = atoi(msg);

    /* Recupero o total da saida*/
    ret = parser(msg,recvbuf,(char *)"&Exits="," ");
    if(ret != MSG_ERROR_NONE)
          return ret;

    (*event).exits = atoi(msg);



    return MSG_ERROR_NONE;

}
