#include "HellaManager.h"


#include <iostream>
#include <cstring>
#include <ctime>

using namespace std;

HellaManager::HellaManager():HellaConnect()
{

}

HellaManager::~HellaManager()
{

}


int HellaManager::openConnection(const char *hostAddr, const char *dstAddr,
                           const unsigned int uPort,const unsigned int timeout_sec)
{
    this->HellaConnect::openConnection(hostAddr,dstAddr,uPort,timeout_sec);

    return this->HellaConnect::state;
}


int HellaManager::retrieveConnectionState()
{
    return this->HellaConnect::state;
}

void HellaManager::closeConnection()
{
    this->HellaConnect::closeConnection();
}

int HellaManager::retrieveSystemHealth(MsgNmGetHealth_t *health)
{

    int ret = MSG_ERROR_NONE;
    uint8_t *msgRawBuffer[MAX_MSG_LEN];
    MsgNmGetConfiguration_t *packetRequestCE = (MsgNmGetConfiguration_t *)msgRawBuffer;

    memset(msgRawBuffer, 0, sizeof(msgRawBuffer));
    initMsgNmGetConfiguration(packetRequestCE);
    packetRequestCE->eReqID = MC_MSG_NM_HEALTH; //System Health

    ret = (MSG_CODES)this->HellaSendMessage((const char *)packetRequestCE,sizeof(MsgNmGetConfiguration_t),this->hSocket);
    if(ret != MSG_ERROR_NONE)
        return ret;

    memset(msgRawBuffer, 0, sizeof(msgRawBuffer));
    const MsgNmGetHealth_t *packetResponseCE = (const MsgNmGetHealth_t *)msgRawBuffer;
    ret = (MSG_CODES)this->HellaRecvMessage((char *)&msgRawBuffer,sizeof(MsgNmGetHealth_t),this->hSocket);
    if(ret != MSG_ERROR_NONE)
        return ret;


    ret = (MSG_CODES)this->HellaCheckHeaderResponse(&packetResponseCE->tHeader,MC_MSG_NM_HEALTH);
    if(ret != MSG_ERROR_NONE)
        return ret;

    *health = *packetResponseCE;

    return MSG_ERROR_NONE;
}

int HellaManager::setDateAndTime(const uint32_t timestamp)
{

    int ret = MSG_ERROR_NONE;
    uint8_t *msgRawBuffer[MAX_MSG_LEN];
    memset(msgRawBuffer, 0, sizeof(msgRawBuffer));
    MsgNmSystemTime_t *packetCommandST = (MsgNmSystemTime_t *)msgRawBuffer;
    initMsgNmSystemTime(packetCommandST,NM_SET_SYSTEM_TIME);
    packetCommandST->uLinuxSystemTime = timestamp;
    ret = (MSG_CODES)this->HellaSendMessage((const char *)packetCommandST,sizeof(MsgNmSystemTime_t),this->hSocket);
    if(ret != MSG_ERROR_NONE)
        return ret;


    const MsgHandshakeResponse_t *packetResponseHS = (const MsgHandshakeResponse_t *)msgRawBuffer;
    memset(msgRawBuffer, 0, sizeof(msgRawBuffer));

    ret = (MSG_CODES)this->HellaRecvMessage((char *)&msgRawBuffer,sizeof(MsgNmParamChanged_t),this->hSocket);
    if(ret != MSG_ERROR_NONE)
        return ret;

    const MsgNmParamChanged_t *packetResponseST = (const MsgNmParamChanged_t *)msgRawBuffer;

    ret = (MSG_CODES)this->HellaCheckHeaderResponse(&packetResponseHS->tHeader,MC_MSG_NM_PARAM_CHANGED);
    if(ret != MSG_ERROR_NONE)
        return ret;

    if (packetResponseST->ChangedParamMsgID != NM_SET_SYSTEM_TIME)
    {
      #ifdef DEBUG_ENABLED
      cout << "Acknowledge of system time change failed" << endl;
      #endif

      ret = MSG_ERROR_SET_DATETIME;
      return ret;
    }

    #ifdef DEBUG_ENABLED
    cout << "Target system time successful synchronized" << endl;
    #endif

    return MSG_ERROR_NONE;
}


int HellaManager::retrieveDateAndTime(uint32_t *timestamp)
{
    if(!timestamp)
        return MSG_ERROR_NULL_POINTER;

    int ret = MSG_ERROR_NONE;
    uint8_t *msgRawBuffer[MAX_MSG_LEN];
    MsgNmGetConfiguration_t *packetRequestCE = (MsgNmGetConfiguration_t *)msgRawBuffer;
    const MsgNmSystemTime_t *packetResponseCE = (const MsgNmSystemTime_t *)msgRawBuffer;

    memset(msgRawBuffer, 0, sizeof(msgRawBuffer));
    initMsgNmGetConfiguration(packetRequestCE);
    packetRequestCE->eReqID = MC_MSG_SYSTEM_TIME_UTC;

    ret = (MSG_CODES)this->HellaSendMessage((const char *)packetRequestCE,sizeof(MsgNmGetConfiguration_t),this->hSocket);
    if(ret != MSG_ERROR_NONE)
        return ret;

    memset(msgRawBuffer, 0, sizeof(msgRawBuffer));
    ret = (MSG_CODES)this->HellaRecvMessage((char *)&msgRawBuffer,sizeof(MsgNmSystemTime_t),this->hSocket);
    if(ret != MSG_ERROR_NONE)
        return ret;

    ret = (MSG_CODES)this->HellaCheckHeaderResponse(&packetResponseCE->tHeader,MC_MSG_SYSTEM_TIME_UTC);
    if(ret != MSG_ERROR_NONE)
        return ret;

    *timestamp = packetResponseCE->uLinuxSystemTime;

    return MSG_ERROR_NONE;
}


int HellaManager::retrieveCounter(int *in, int *out)
{
    int ret = MSG_ERROR_NONE;
    uint8_t *msgRawBuffer[MAX_MSG_LEN];
    MsgNmGetConfiguration_t *packetRequestCE = (MsgNmGetConfiguration_t *)msgRawBuffer;
    const MsgCountEvent_t *packetResponseCE = (const MsgCountEvent_t *)msgRawBuffer;

    memset(msgRawBuffer, 0, sizeof(msgRawBuffer));
    initMsgNmGetConfiguration(packetRequestCE);
    packetRequestCE->eReqID = MC_MSG_COUNT_EVENT;

    ret = (MSG_CODES)this->HellaSendMessage((const char *)packetRequestCE,sizeof(MsgNmGetConfiguration_t),this->hSocket);
    if(ret != MSG_ERROR_NONE)
        return ret;

    memset(msgRawBuffer, 0, sizeof(msgRawBuffer));
    ret = (MSG_CODES)this->HellaRecvMessage((char *)&msgRawBuffer,sizeof(MsgCountEvent_t),this->hSocket);
    if(ret != MSG_ERROR_NONE)
        return ret;

    ret = (MSG_CODES)this->HellaCheckHeaderResponse(&packetResponseCE->tHeader,MC_MSG_COUNT_EVENT);
    if(ret != MSG_ERROR_NONE)
        return ret;

    *in = packetResponseCE->uCountIn;
    *out = packetResponseCE->uCountOut;

    return MSG_ERROR_NONE;
}

int HellaManager::resetCounter()
{
    return sendCommandToDevice(NM_RESET_COUNT_RESULTS,NULL);
}

int HellaManager::reboot(const uint32_t mode)
{
    return sendCommandToDevice(NM_SYSTEM_REBOOT,&mode);
}


int HellaManager::startStopCounter(const uint32_t mode)
{
    return sendCommandToDevice(NM_START_STOP_COUNTING,&mode);
}


int HellaManager::sendCommandToDevice(const uint32_t COMMAND_ID, const uint32_t *value)
{
    int ret = MSG_ERROR_NONE;
    uint8_t *msgRawBuffer[MAX_MSG_LEN];

    MsgNmCommand_t *packetRequestCE = (MsgNmCommand_t *)msgRawBuffer;
    const MsgNmParamChanged_t *packetResponseCE = (const MsgNmParamChanged_t *)msgRawBuffer;

    memset(msgRawBuffer, 0, sizeof(msgRawBuffer));
    initMsgNmCommand(packetRequestCE,COMMAND_ID);

    if(value)
        packetRequestCE->value = *value;

    ret = (MSG_CODES)this->HellaSendMessage((const char *)packetRequestCE,sizeof(MsgNmCommand_t),this->hSocket);
    if(ret != MSG_ERROR_NONE)
        return ret;


    memset(msgRawBuffer, 0, sizeof(msgRawBuffer));
    ret = (MSG_CODES)this->HellaRecvMessage((char *)&msgRawBuffer,sizeof(MsgNmCommand_t),this->hSocket);
    if(ret != MSG_ERROR_NONE)
        return ret;

    ret = (MSG_CODES)this->HellaCheckHeaderResponse(&packetResponseCE->tHeader,MC_MSG_NM_PARAM_CHANGED);
    if(ret != MSG_ERROR_NONE)
        return ret;

    if (packetResponseCE->ChangedParamMsgID != COMMAND_ID)
    {
        #ifdef DEBUG_ENABLED
        cout << endl << "Command execution failed." << endl;
        #endif

        ret = MSG_ERROR_COMMAND_EXEC;
        return ret;
    }



    return MSG_ERROR_NONE;
}

int HellaManager::retrieveSystemStatus(MsgNmStatus_t *status)
{

    int ret = MSG_ERROR_NONE;
    uint8_t *msgRawBuffer[MAX_MSG_LEN];
    MsgNmGetConfiguration_t *packetRequestCE = (MsgNmGetConfiguration_t *)msgRawBuffer;

    memset(msgRawBuffer, 0, sizeof(msgRawBuffer));
    initMsgNmGetConfiguration(packetRequestCE);
    packetRequestCE->eReqID = MC_MSG_NM_STATUS; //System Health

    ret = (MSG_CODES)this->HellaSendMessage((const char *)packetRequestCE,sizeof(MsgNmGetConfiguration_t),this->hSocket);
    if(ret != MSG_ERROR_NONE)
        return ret;

    memset(msgRawBuffer, 0, sizeof(msgRawBuffer));
    const MsgNmStatus_t *packetResponseCE = (const MsgNmStatus_t *)msgRawBuffer;
    ret = (MSG_CODES)this->HellaRecvMessage((char *)&msgRawBuffer,sizeof(MsgNmStatus_t),this->hSocket);
    if(ret != MSG_ERROR_NONE)
        return ret;


    ret = (MSG_CODES)this->HellaCheckHeaderResponse(&packetResponseCE->tHeader,MC_MSG_NM_STATUS);
    if(ret != MSG_ERROR_NONE)
        return ret;

    *status = *packetResponseCE;

    return MSG_ERROR_NONE;
}
